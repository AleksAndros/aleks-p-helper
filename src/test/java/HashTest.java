import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.paukov.combinatorics3.Generator;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Disabled
public class HashTest {

    @Test
    public void hashCodes() {
        Deck deck = new Deck();
        Set<Integer> s = deck.stream().map(Card::hashCode).collect(Collectors.toSet());
        assertEquals(52, s.size());
    }

    @Test
    public void cardsHashCodes() {
        Deck deck = new Deck();
        List<Cards> cardsList = Generator.combination(deck).simple(4).stream().map(Cards::new).collect(Collectors.toList());
        Set<Integer> s = cardsList.stream().map(Cards::hashCode).collect(Collectors.toSet());
        assertEquals((int) umontreal.iro.lecuyer.util.Num.combination(deck.size(),4), s.size());
    }

    @Test
    public void fullHandsHashCodes() {
        Deck deck = new Deck();
        List<FullHand> cardsList = Generator.combination(deck).simple(5).stream().map(FullHand::new).collect(Collectors.toList());
        Set<Integer> s = cardsList.stream().map(FullHand::hashCode).collect(Collectors.toSet());
        int expected = (int) umontreal.iro.lecuyer.util.Num.combination(deck.size(),5);
        if(cardsList.size() < expected + 10000 || cardsList.size() > expected - 10000){
            assert(true);
        }
        assert(false);
    }

    @Test
    public void generatorCombinationHashCodes() {
        Deck deck = new Deck();
        List<FullHand> cardsList = Generator.combination(deck).simple(5).stream().map(FullHand::new).collect(Collectors.toList());
        Set<Integer> s = cardsList.stream().map(FullHand::hashCode).collect(Collectors.toSet());
        assertEquals((int) umontreal.iro.lecuyer.util.Num.combination(52,5), cardsList.size());
    }
}
