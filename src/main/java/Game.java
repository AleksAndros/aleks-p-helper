import org.paukov.combinatorics3.Generator;
import umontreal.iro.lecuyer.randvar.UniformIntGen;
import umontreal.iro.lecuyer.rng.LFSR113;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alessandro on 20/09/2017.
 */
public class Game {
    protected Board board;
    protected List<Player> players = new ArrayList<>();
    private Deck deck = new Deck();
    private Round round = Round.PREFLOP;
    private int pot = 0;
    private int ante = 0;
    private int bigBlind = 0;
    private Random rand = new Random(System.currentTimeMillis());

    public Game (Player you, Board board, Integer numberOfPlayers){
        this.board = board;
        this.setAppropriateRound();
        this.players.add(you);
        you.setActive();
        for (int i = 0; i<numberOfPlayers-1; i++){
            this.players.add(new Player());
        }
        for (Player player : this.players) {
            player.setGame(this);
        }
        this.updatePlayerDistribution();
        this.deck.remove(you.getHand().getFirstCard());
        this.deck.remove(you.getHand().getSecondCard());
        this.deck.removeAll(board);
    }

    public Game(ImageHandler imageHandler) {
        this.pot = imageHandler.getPot();
        this.setBigBlind(imageHandler.getBigBlind());
        this.board = imageHandler.getBoard();
        this.setAppropriateRound();
        Player you = new Player("Alessandro", imageHandler.getHand());
        you.setActive();
        this.players.add(you);
        for(Player opponent : imageHandler.getPlayers()){
            if(opponent.getStack() != -1) this.players.add(opponent);
        }
        for (Player player : this.players) {
            player.setGame(this);
        }
        this.updatePlayerDistribution();
        this.deck.remove(you.getHand().getFirstCard());
        this.deck.remove(you.getHand().getSecondCard());
        this.deck.removeAll(board);
    }

    public Game(Game game) {
        this.pot = game.pot;
        this.setBigBlind(game.getBigBlind());
        this.board = game.board.copy();
        this.setAppropriateRound();
        game.players.forEach(player -> this.players.add(new Player(player,this)));
        this.deck = new Deck(game.deck);
    }

    public Game() {}

    public void setAppropriateRound() {
        if(this.board.size() == 0) this.round = Round.PREFLOP;
        if(this.board.size() == 3) this.round = Round.FLOP;
        if(this.board.size() == 4) this.round = Round.TURN;
        if(this.board.size() == 5) this.round = Round.RIVER;
    }

    public int getCurrentBet() {
        Integer best = 0;
        int maxBet = 0;
        for(Player p : this.getActivePlayers()){
            int candidate = p.getCurrentBet();
            if(candidate > best) {
                best = candidate;
            }
        }
        return maxBet;
    }

    public int getPot() {
        return pot;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public int getSmallBlind() {
        return bigBlind/2;
    }

    public Cards getCardsOnTable(Player player){
        Cards cardsOnTable = new Cards(this.board);
        cardsOnTable.add(player.getHand().getFirstCard());
        cardsOnTable.add(player.getHand().getSecondCard());
        return cardsOnTable;
    }

    public FullHand getBestCombinationOfCardsOnTableFor(Player player) {
        Cards cardsOnTable = this.getCardsOnTable(player);

//        System.out.println(cardsOnTable);
        List<FullHand> fhs = Generator.combination(cardsOnTable).simple(5).stream().map(FullHand::new).collect(Collectors.toList());
        return Main.db.getAnyBestAmongFullHands(fhs);
    }

    private FullHand getBestCombinationOfCardsOnTableContainingAtLeastOneCardFor(Player player) {
        Cards cardsOnTable = this.getCardsOnTable(player);

//        System.out.println(cardsOnTable);
        List<FullHand> fhs = Generator.combination(cardsOnTable).simple(5).stream().filter(cards -> cards.contains(player.getHand().getFirstCard()) || cards.contains(player.getHand().getFirstCard())).map(FullHand::new).collect(Collectors.toList());
        return Main.db.getAnyBestAmongFullHands(fhs);
    }

    public List<Player> getCurrentWinners() {
        if(this.board.size()<3) return new ArrayList<>();

        this.getActivePlayers().forEach(p->p.setFullHand(this.getBestCombinationOfCardsOnTableFor(p)));
        List<FullHand> list = this.getActivePlayers().stream().map(Player::getFullHand).collect(Collectors.toList());
        List<FullHand> bestFullHands = Main.db.getBestAmongFullHands(list);

        return this.getActivePlayers().stream().filter(p -> bestFullHands.contains(p.getFullHand())).collect(Collectors.toList());
    }

    public Probabilities getProbabilitiesBasedOnOutsFor(Player player){
        FullHand bestOpponentHand = this.getBestCombinationOfCardsOnTableFor(player);

        double basicWinProbabilities = 0;

        int outs = 0;
        for(Card c : this.deck) {
            if(!this.board.containsSameRankOf(c)) {
                this.board.add(c);
                FullHand bestOpponentHandNow = this.getBestCombinationOfCardsOnTableContainingAtLeastOneCardFor(player);
                if (bestOpponentHandNow.getPoint().getRanking().getRanking() > bestOpponentHand.getPoint().getRanking().getRanking())
                    outs++;
                this.board.remove(c);
            }
        }
//        System.out.println("Outs: " + outs);

        if(this.round == Round.FLOP){
            switch (outs){
                case 0:
                    break;
                case 1:
                    basicWinProbabilities += 0.043;
                    break;
                case 2:
                    basicWinProbabilities += 0.084;
                    break;
                case 3:
                    basicWinProbabilities += 0.125;
                    break;
                case 4:
                    basicWinProbabilities += 0.165;
                    break;
                case 5:
                    basicWinProbabilities += 0.204;
                    break;
                case 6:
                    basicWinProbabilities += 0.241;
                    break;
                case 7:
                    basicWinProbabilities += 0.278;
                    break;
                case 8:
                    basicWinProbabilities += 0.315;
                    break;
                case 9:
                    basicWinProbabilities += 0.350;
                    break;
                case 10:
                    basicWinProbabilities += 0.384;
                    break;
                case 11:
                    basicWinProbabilities += 0.417;
                    break;
                case 12:
                    basicWinProbabilities += 0.450;
                    break;
                case 13:
                    basicWinProbabilities += 0.481;
                    break;
                case 14:
                    basicWinProbabilities += 0.512;
                    break;
                case 15:
                    basicWinProbabilities += 0.541;
                    break;
                case 16:
                    basicWinProbabilities += 0.570;
                    break;
                case 17:
                    basicWinProbabilities += 0.598;
                    break;
                case 18:
                    basicWinProbabilities += 0.624;
                    break;
                case 19:
                    basicWinProbabilities += 0.650;
                    break;
                case 20:
                    basicWinProbabilities += 0.675;
                    break;
                case 21:
                    basicWinProbabilities += 0.699;
                    break;
                case 22:
                    basicWinProbabilities += 0.722;
                    break;
                default:
                    System.out.println("More than 22 outs!");
                    break;
            }
        }
        else if(this.round == Round.TURN){
            switch (outs){
                case 0:
                    break;
                case 1:
                    basicWinProbabilities += 0.022;
                    break;
                case 2:
                    basicWinProbabilities += 0.043;
                    break;
                case 3:
                    basicWinProbabilities += 0.065;
                    break;
                case 4:
                    basicWinProbabilities += 0.087;
                    break;
                case 5:
                    basicWinProbabilities += 0.109;
                    break;
                case 6:
                    basicWinProbabilities += 0.130;
                    break;
                case 7:
                    basicWinProbabilities += 0.152;
                    break;
                case 8:
                    basicWinProbabilities += 0.174;
                    break;
                case 9:
                    basicWinProbabilities += 0.196;
                    break;
                case 10:
                    basicWinProbabilities += 0.217;
                    break;
                case 11:
                    basicWinProbabilities += 0.239;
                    break;
                case 12:
                    basicWinProbabilities += 0.261;
                    break;
                case 13:
                    basicWinProbabilities += 0.283;
                    break;
                case 14:
                    basicWinProbabilities += 0.304;
                    break;
                case 15:
                    basicWinProbabilities += 0.326;
                    break;
                case 16:
                    basicWinProbabilities += 0.348;
                    break;
                case 17:
                    basicWinProbabilities += 0.370;
                    break;
                case 18:
                    basicWinProbabilities += 0.391;
                    break;
                case 19:
                    basicWinProbabilities += 0.413;
                    break;
                case 20:
                    basicWinProbabilities += 0.435;
                    break;
                case 21:
                    basicWinProbabilities += 0.457;
                    break;
                case 22:
                    basicWinProbabilities += 0.478;
                    break;
                default:
                    System.out.println("More than 22 outs!");
                    break;
            }
        }

        return new Probabilities(basicWinProbabilities,0,1-basicWinProbabilities);
    }

    private int getRandomSeed(){
        int r = rand.nextInt();
        while(r == 0) r = rand.nextInt();
        return -Math.abs(r);
    }

    public void updatePlayerDistribution() {
        // Refine distributions based on round and bets

        if(this.round.equals(Round.PREFLOP)){
            for(Player opponent : players){
                if(!opponent.isActive()) opponent.setHandDistributionBelow(0.5);
                else {
                    if(opponent.isAllIn()){
                        opponent.setHandDistributionAbove(0.66);
                    }
                    else if(opponent.getCurrentBet() >= 3*bigBlind){
                        opponent.setHandDistributionAbove(0.5);
                    }
                    else if(opponent.getCurrentBet() >= bigBlind){
                        opponent.setHandDistributionAbove(0.45);
                    }
                }
            }
        }
        else if(this.round.equals(Round.FLOP) || this.round.equals(Round.TURN) || this.round.equals(Round.RIVER)){
            for(Player opponent : getActivePlayers()){
//                if(opponent.getCurrentBet() > this.getPot()/3) {
                if(getActivePlayers().indexOf(opponent) != 0) opponent.setHandDistribution(opponent.getHandsThatCanCallTo(opponent.getCurrentBet()));
//                }
            }
        }
    }

    public Probabilities getProbabilitiesFor(Player player){
        return this.getProbabilitiesFor(player,1000);
    }

    public Probabilities getProbabilitiesFor(Player player, int maxIterations){
        Probabilities p = new Probabilities(0,0,0);

        List<Player> playersWithoutCards = players.stream().filter(f -> !f.hasKnownCards()).collect(Collectors.toList());
        Integer numberOfPlayerWithoutCards = playersWithoutCards.size();
        if(this.board.isFull() && numberOfPlayerWithoutCards == 0){
            List<Player> winners = this.getCurrentWinners();
            if(!winners.contains(player)) return new Probabilities(0,0,1);
            if(winners.size() == 1) return new Probabilities(1,0,0);
            else return new Probabilities(0,1,0);
        }

        Board originalBoard = this.board.copy();
        Probabilities simulationProbabilities;

        for(int iteration = 0; iteration<maxIterations; iteration++){
            Deck d = new Deck();
            d.removeAll(this.board);
            d.removeAll(this.getYou().getCards());

            // Choose randomly from all distributions and deck for board (after eliminating opponents' cards (FOLDS INCLUDED))
            for(Player opponent : players){
                if(players.indexOf(opponent) == 0 ) continue;
                LFSR113.setPackageSeed(new int[]{this.getRandomSeed(), this.getRandomSeed(), this.getRandomSeed(), this.getRandomSeed()});
                UniformIntGen rangeStreamForPlayers = new UniformIntGen(new LFSR113(), 0, opponent.getHandDistribution().size()-1);
                Hand opponentHand;
                while(!opponent.isValid()){
                    opponentHand = new Hand(opponent.getHandDistribution().get(rangeStreamForPlayers.nextInt()));
                    if(d.containsAll(opponentHand.getCards())) opponent.setHand(opponentHand);
                }
                d.removeAll(opponent.getCards());
            }
            while(!this.board.isFull()){
                LFSR113.setPackageSeed(new int[]{this.getRandomSeed(), this.getRandomSeed(), this.getRandomSeed(), this.getRandomSeed()});
                UniformIntGen rangeStreamForBoard = new UniformIntGen(new LFSR113(), 0, d.size()-1);
                Card boardCard = d.get(rangeStreamForBoard.nextInt());
                this.board.add(boardCard);
                d.remove(boardCard);
            }

            // Calculate probability as usual
            simulationProbabilities = this.getProbabilitiesFor(player, maxIterations);

            // Revert changes to board and players
            this.board = originalBoard.copy();
            for (Player player1 : players) {
                if(players.indexOf(player1) == 0 ) continue;
                player1.clearHand();
            }

            p.addSimulation(simulationProbabilities);
        }

        return p;
    }

    @Override
    public String toString() {
        String gameRepresentation = "\n" + this.getSmallBlind() + "/" + this.bigBlind + "\n" + "Pot: " + this.pot + "\n\n";
        StringBuilder gameRepresentationBuilder = new StringBuilder();
        for (Player player : this.players) {
            gameRepresentationBuilder
                    .append(player.getName())
                    .append(": ")
                    .append(player.getHand())
                    .append(getActivePlayers().contains(player) ? "" : "\t FOLDED")
                    .append("\n");
        }
        gameRepresentation += gameRepresentationBuilder.toString();
        gameRepresentation += "\nBoard: " + this.board.toString() + "\n\n";
//        gameRepresentation += "\n\n" + this.players.get(this.getPosition()) + "'s turn";
        return gameRepresentation;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<Player> getActivePlayers() {
        return this.players.stream().filter(Player::isActive).collect(Collectors.toList());
    }

    public String getWinners() {
        return "Winners: " +
                this.getCurrentWinners().stream().map(Player::getName)
                        .collect(Collectors.toList())
                + "\n\n";
    }

//    public void nextPosition() {
//
//        this.position = (this.position+1) % this.players.size();
//        while(this.foldedPlayers.contains(this.position) && this.getActivePlayers().size() > 1){
//            this.position = (this.position+1) % this.players.size();
//        }
//
//        this.getActivePlayers().forEach(Player::resetProbabilities);
//
//        if(this.turnsFinished() && this.proceedOnFurtherRounds) { // Next round
//
//            this.setCurrentBet(0);
//            this.position = (this.dealer+1) % this.players.size();
//            while(this.foldedPlayers.contains(this.position) && this.getActivePlayers().size() > 1){
//                this.position = (this.position+1) % this.players.size();
//            }
//            this.lastPlayerWhoSpoke = this.position;
//
//            this.getActivePlayers().forEach(player -> player.setAlreadyBet(0));
//            this.round = this.round.nextRound();
//            if (this.round != null && this.getActivePlayers().size() > 1) {
//                if(this.round.equals(Round.FLOP)){
//                    this.addCardsOnBoard(3);
//                }
//                else if(this.round.equals(Round.TURN) || this.round.equals(Round.RIVER)){
//                    this.addCardsOnBoard(1);
//                }
//                if(this.verbose) System.out.println(this);
//            }
//
//            if(this.getActivePlayers().stream().allMatch(player -> player.getStack() == 0) && !this.isFinished()) { // All active are all-in.
//                this.nextPosition();
//            }
//            else if(this.isFinished() && verbose){
//                System.out.println(this.getWinners());
//            }
//        }
//    }

//    public void addCardsOnBoard(int numberOfCards) {
//        CombinationIterator c = new CombinationIterator(this.deck, numberOfCards);
//        List<Card> cards = c.next();
//        this.deck.removeAll(cards);
//        this.board.addAll(cards);
//    }
//
//    public void addCardsOnBoard(List<Card> cards) {
//        this.deck.removeAll(cards);
//        this.board.addAll(cards);
//    }

    public void setPot(int pot) {
        this.pot = pot;
    }

    public void setBigBlind(int bigBlind) {
        this.bigBlind = bigBlind;
    }

    public Round getRound() {
        return round;
    }

    public boolean isValid() {
        return this.getYou().isValid();
    }

    public Deck getDeck() {
        return deck;
    }

    public Player getYou() {
        return this.players.get(0);
    }

    public void updateWith(ImageHandler imageHandler) {
        this.pot = imageHandler.getPot();
        this.board = imageHandler.getBoard();
        this.setAppropriateRound();
        for(Player opponent : imageHandler.getPlayers()){
            Player thisOpponent = this.getPlayerWithName(opponent.getName());
            if (thisOpponent != null) {
                thisOpponent.setStack(opponent.getStack());
                thisOpponent.setCurrentBet(opponent.getCurrentBet());
            }
        }
        this.updatePlayerDistribution();
        this.deck.removeAll(board);

    }

    private Player getPlayerWithName(String name) {
        for(Player p : this.players){
            if(p.getName().equals(name)) return p;
        }
        return null;
    }

    public Board getBoard() {
        return board;
    }

    public void play() {
        GameHandler gameHandler = new GameHandler(this);
        while(!gameHandler.isGameEnded()){
            gameHandler.proceed();
        }
    }
}
