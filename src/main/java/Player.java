import org.paukov.combinatorics3.Generator;
import umontreal.iro.lecuyer.util.Chrono;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Alessandro on 20/09/2017.
 */
public class Player {
    private Hand hand = new Hand();
    private int position;
    private FullHand fullHand;
    private int stack;
    private String name;
    private Game game;
    private Probabilities probabilities;
    private List<Hand> handDistribution;
    private boolean isActive = false;
    private boolean isAllIn;
    private int currentBet = 0;

    public Player() {}

    public Player(Hand hand) {
        this.hand = hand;
    }

    public Player (String name, Hand hand){
        this.name = name;
        this.hand = hand;
    }

    public Player(Player player, Game game) {
        this.hand = player.hand;
        this.position = player.position;
        this.stack = player.stack;
        this.name = player.name;
        this.game = game;
        this.probabilities = player.probabilities;
        this.currentBet = player.currentBet;
        this.probabilities = player.probabilities;
        this.isActive = player.isActive;
        this.isAllIn = player.isAllIn;
        this.handDistribution = player.handDistribution;
    }

    public Player(List<Hand> handDistribution) {
        this.handDistribution = handDistribution;
    }

    public void setUniformHandDistribution(){
        if(!this.game.getYou().equals(this)) this.handDistribution = Main.db.rankingPerHand.keySet().stream().filter(hand -> !this.game.getBoard().contains(hand.getFirstCard()) && !this.game.getBoard().contains(hand.getSecondCard()) && !this.game.getYou().getCards().contains(hand.getFirstCard()) && !this.game.getYou().getCards().contains(hand.getSecondCard())).collect(Collectors.toList());
        else this.handDistribution = new ArrayList<>(Main.db.rankingPerHand.keySet());
    }

    public void setHandDistributionAbove(double threshold){
        this.handDistribution.retainAll(Main.db.getHandsAbove(threshold).stream().filter(hand1 -> this.game.getDeck().containsAll(hand1.getCards())).collect(Collectors.toList()));
    }

    public void setHandDistributionBelow(double threshold){
        this.handDistribution.retainAll(Main.db.getHandsBelow(threshold).stream().filter(hand1 -> this.game.getDeck().containsAll(hand1.getCards())).collect(Collectors.toList()));
    }

    public Hand getHand() {
        return hand;
    }

    public FullHand getFullHand() {
        return fullHand;
    }

    public String getName() {
        return name;
    }

    public void setHand(Card card1, Card card2) {
        this.hand = new Hand(card1, card2);
    }

    public void setGame(Game game) {
        this.game = game;
        if(this.name == null) this.name = "Opponent #" + (this.game.getPlayers().indexOf(this));
        this.setUniformHandDistribution();
    }

    public boolean hasKnownCards() {
        return this.hand != null && this.hand.getFirstCard() != null && this.hand.getSecondCard() != null;
    }

    public void clearHand() {
        this.hand.clearHand();
    }

    @Override
    public String toString() {
        return this.name != null ? this.name : ("Opponent #" + (this.game.getPlayers().indexOf(this)+1));
    }

    public int getStack() {
        return stack;
    }

    public void setFullHand(FullHand fullHand) {
        this.fullHand = fullHand;
    }

    public boolean isValid() {
        return this.hand.isValid();
    }

    public Cards getCards() {
        return this.hand.getCards();
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public List<Hand> getHandDistribution() {
        return handDistribution;
    }

    public boolean isActive() {
        return this.isActive;
    }

    public boolean isAllIn() {
        return isAllIn;
    }

    public void setCurrentBet(int currentBet) {
        this.currentBet = currentBet;
        if(currentBet == -1) this.currentBet = 0;
    }

    public void setActive() {
        this.isActive = true;
    }

    public int getCurrentBet() {
        return currentBet;
    }

    public void setStack(int stack) {
        this.stack = stack;
        if(this.stack == -1) {
            if(this.game != null && this.game.getPlayers() != null) this.game.getPlayers().remove(this);
        }
        if(this.stack == -2){
            this.stack = 0;
            this.isAllIn = true;
        }
    }
    public List<Hand> getHandsThatCanCallTo (int amountToPut){
        return handDistribution.stream().filter(hand1 -> {
            Player opponent = new Player(hand1);
            opponent.setActive();
            Game g2 = new Game(this.game);
            g2.getPlayers().set(0,opponent);

            if(this.game.getPlayers().indexOf(this) != 0) {
                Player myClone = new Player(Main.db.rankingPerHand.keySet().stream().filter(hand2 -> !g2.getBoard().contains(hand2.getFirstCard()) && !g2.getBoard().contains(hand2.getSecondCard()) && !hand2.getCards().contains(hand1.getFirstCard()) && !hand2.getCards().contains(hand1.getSecondCard())).collect(Collectors.toList()));
                myClone.setActive();
                g2.getPlayers().set(this.game.getPlayers().indexOf(this), myClone);
            }
            g2.getDeck().addAll(this.game.getYou().getCards());
            g2.getDeck().removeAll(hand1.getCards());

//            System.out.println(g2);

            // Made hands
            if(g2.getBestCombinationOfCardsOnTableFor(opponent).getPoint().getRanking().getRanking()>=2) return true;
            // Draws
            Probabilities p = g2.getProbabilitiesBasedOnOutsFor(opponent);
            return p.getWinning()*(g2.getPot()+amountToPut) - amountToPut > 0;

        }).collect(Collectors.toList());
    }

    public void setHandDistribution(List<Hand> handDistribution) {
        this.handDistribution = handDistribution;
    }
}
