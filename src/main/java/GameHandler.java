public class GameHandler {
    private Game game;

    public GameHandler(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public boolean isGameEnded() {
        return false;
    }

    public void proceed() {

    }
}
