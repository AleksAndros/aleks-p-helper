public enum ActionType {
    FOLD,
    CHECK,
    BET,
    CALL,
    RAISE,
    ALLIN,
    UNKNOWN
}
