public class Action {
    protected ActionType actionType;
    private double amount;

    public Action(ActionType actionType, double amount){
        this.actionType = actionType;
        this.amount = amount;
    }

    public ActionType getActionType() {
        return actionType;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return this.actionType.name() + " " + this.amount;
    }
}
