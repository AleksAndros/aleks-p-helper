import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Alessandro on 20/09/2017.
 */
public class Hand {
    private Card firstCard;
    private Card secondCard;

    public Hand (Card firstCard, Card secondCard){
        this.firstCard = firstCard;
        this.secondCard = secondCard;
    }

    public Hand(List<Card> cards) {
        this.firstCard = cards.get(0);
        this.secondCard = cards.get(1);
    }

    public Hand() {}

    public Hand(String handRep) {
        firstCard = new Card(handRep.substring(0,2));
        secondCard = new Card(handRep.substring(2,4));
    }

    public Hand(Hand hand) {
        this.firstCard = new Card(hand.firstCard);
        this.secondCard = new Card(hand.secondCard);
    }

    public Hand(Cards cards) {
        this(new ArrayList<>(cards));
    }

    public Card getFirstCard() {
        return firstCard;
    }

    public Card getSecondCard() {
        return secondCard;
    }

    public void setFirstCard(Card firstCard) {
        this.firstCard = firstCard;
    }

    public void setSecondCard(Card secondCard) {
        this.secondCard = secondCard;
    }

    public static ArrayList<Hand> hands(){
        ArrayList<Hand> hands = new ArrayList<>();
        Deck deck = new Deck();
        deck.forEach(firstCard -> deck.forEach(secondCard -> {
            if(!firstCard.equals(secondCard)) {
                Hand hand = new Hand(firstCard,secondCard);
                if(!hands.contains(hand)) hands.add(hand);
            }
        }));
        return hands;
    }

    public void clearHand() {
        this.firstCard = null;
        this.secondCard = null;
    }

    @Override
    public String toString() {
        return (firstCard != null ? firstCard.toString() : "x") + " " + (secondCard != null ? secondCard.toString() : "x");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hand hand = (Hand) o;

        return (this.firstCard.equals(hand.firstCard) && this.secondCard.equals(hand.secondCard)) || (this.firstCard.equals(hand.secondCard) && this.secondCard.equals(hand.firstCard));
    }

    @Override
    public int hashCode() {
        int result = firstCard != null ? firstCard.hashCode() : 0;
        result += secondCard != null ? secondCard.hashCode() : 0;
        return result;
    }

    public Hand copy() {
        return new Hand(this.firstCard, this.secondCard);
    }

    public boolean isValid() {
        return this.firstCard != null && this.secondCard != null && this.firstCard.isValid() && this.secondCard.isValid();
    }

    public Cards getCards() {
        return new Cards(this.getFirstCard(), this.getSecondCard());
    }

    public String dbRep() {
        return this.getCards().dbRep();
    }

    public boolean contains(Card c) {
        return this.firstCard.equals(c) || this.secondCard.equals(c);
    }
}
