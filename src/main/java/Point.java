import java.util.*;
import java.util.stream.Collectors;

public class Point {

    private Ranking ranking;
    private Rank strongestRankOfPoint;
    private List<Rank> kickers;

    public Point(Ranking ranking, Rank strongestRankOfPoint, List<Rank> kickers){
        this.ranking = ranking;
        this.strongestRankOfPoint = strongestRankOfPoint;
        this.kickers = kickers;
    }
    public Point(Ranking ranking, Rank strongestRank) {
        this.ranking = ranking;
        this.strongestRankOfPoint = strongestRank;
    }

    public Point(String r) {
        this.ranking = Ranking.getRankingFromInt(Integer.parseInt(String.valueOf(r.charAt(0))));
        this.strongestRankOfPoint = Rank.getRankFromChar(r.charAt(1));
        if(r.length() > 2) {
            this.kickers = new ArrayList<>();
            for (int i = 2; i < r.length(); i++) {
                this.kickers.add(Rank.getRankFromChar(r.charAt(i)));
            }
        }
    }

    public Ranking getRanking() {
        return ranking;
    }

    public Rank getStrongestRankOfPoint() {
        return strongestRankOfPoint;
    }

    public List<Rank> getKickers() {
        return kickers;
    }

    public void setRanking(Ranking ranking) {
        this.ranking = ranking;
    }

    public void setStrongestRankOfPoint(Rank strongestRankOfPoint) {
        this.strongestRankOfPoint = strongestRankOfPoint;
    }

    public void setKickers(List<Rank> kickers) {
        this.kickers = kickers;
    }

    public int against(Point point){ // -1 = LOSES, 0 = TIES, 1 = WINS

        Integer result = 0;

        if(this.ranking.getRanking() > point.getRanking().getRanking()) result = 1;
        else if(this.ranking.getRanking() < point.getRanking().getRanking()) result = -1;

        if(this.strongestRankOfPoint != null && result == 0)
        {
            if(this.strongestRankOfPoint.getRank() > point.strongestRankOfPoint.getRank()) result = 1;
            else if(this.strongestRankOfPoint.getRank() < point.strongestRankOfPoint.getRank()) result = -1;
        }

        if(this.ranking.equals(Ranking.TWO_PAIR) && result == 0){
            Map<Integer, Rank> numberOfRanks = new HashMap<>();
            this.kickers.stream().collect(Collectors.groupingBy(Rank::getRank))
                    .forEach((r,l) -> numberOfRanks.put(l.size(), l.get(0)));

            Rank secondPairRank = numberOfRanks.get(2); // If it's TWO_PAIR, then there HAS TO be a 2.

            Map<Integer, Rank> otherPointNumberOfRanks = new HashMap<>();
            point.kickers.stream().collect(Collectors.groupingBy(Rank::getRank))
                    .forEach((r,l) -> otherPointNumberOfRanks.put(l.size(), l.get(0)));

            Rank otherPointSecondPairRank = otherPointNumberOfRanks.get(2);

            if(secondPairRank.getRank() > otherPointSecondPairRank.getRank()) result = 1;
            else if(secondPairRank.getRank() < otherPointSecondPairRank.getRank()) result = -1;
        }

        if(this.kickers != null && result == 0) {
            for(int i=0; i<this.kickers.size(); i++){
                if(this.kickers.get(i).getRank() > point.kickers.get(i).getRank()) result = 1;
                else if(this.kickers.get(i).getRank() < point.kickers.get(i).getRank()) result = -1;
            }
        }

        return result;
    }

    @Override
    public String toString() {
        return this.ranking + (this.strongestRankOfPoint != null ? " OF " +this.strongestRankOfPoint : "")
                + (this.kickers != null ? "\nKickers: " + this.kickers : "");
    }

    @Override
    public int hashCode() {
        int hash = ranking.getRanking();
        hash += strongestRankOfPoint != null ? 10*strongestRankOfPoint.getRank() : 0;
        hash += kickers != null ? 150*kickers.hashCode() : 0;
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        return this.ranking == null || point.getRanking() == null || this.ranking.equals(point.getRanking()) && (this.strongestRankOfPoint == null || point.getStrongestRankOfPoint() == null || this.strongestRankOfPoint.equals(point.getStrongestRankOfPoint()) && (this.kickers == null || point.getKickers() == null || this.kickers.equals(point.getKickers())));
    }

    public String dbRep() {
        StringBuilder s = new StringBuilder();
        s.append(ranking.getRanking().toString()); // From 1 to 9
        s.append(strongestRankOfPoint.toString()); // One character
        if(kickers != null) {
            for (Rank r : kickers) {
                s.append(r.toString());
            }
        }
        return s.toString();
    }
}
