import java.util.ArrayList;

/**
 * Created by Alessandro on 20/09/2017.
 */
public class Board extends ArrayList<Card> {

    public Board() {}

    public Board(Card card) {
        super();
        this.add(card);
    }

    public Board(Card card1, Card card2) {
        super();
        this.add(card1);
        this.add(card2);
    }

    public Board(Card card1, Card card2, Card card3) {
        super();
        this.add(card1);
        this.add(card2);
        this.add(card3);
    }

    public Board(Card card1, Card card2, Card card3, Card card4) {
        super();
        this.add(card1);
        this.add(card2);
        this.add(card3);
        this.add(card4);
    }

    public Board(Card card1, Card card2, Card card3, Card card4, Card card5) {
        super();
        if(card1.isValid()) this.add(card1);
        if(card2.isValid()) this.add(card2);
        if(card3.isValid()) this.add(card3);
        if(card4.isValid()) this.add(card4);
        if(card5.isValid()) this.add(card5);
    }

    public Board(Cards cards) {
        super(cards);
    }

    public Board(Board cards) {
        this();
        this.addAll(cards);
    }

    public Cards getFlop() {
        if(this.size() >= 3) return new Cards(this.subList(0,3));
        else return new Cards(this.subList(0,this.size()));
    }

    public Card getTurn() {
        return this.size() >= 4 ? this.get(3) : new Card();
    }

    public Card getRiver() {
        return this.size() == 5 ? this.get(4) : new Card();
    }

    public void setFlop(Cards flop) {
        this.removeAll(this.getFlop());
        this.addAll(0,flop);
    }

    public void setTurn(Card turn) {
        this.remove(this.get(3));
        this.add(3,turn);
    }

    public void setRiver(Card river) {
        this.remove(this.get(4));
        this.add(4, river);
    }

    @Override
    public String toString() {
        StringBuilder boardRepresentationBuilder = new StringBuilder("| ");
        for(Card card : this.getFlop()){
            boardRepresentationBuilder.append(card.toString()).append(" ");
        }
        String boardRepresentation = boardRepresentationBuilder.toString();
        if(this.size()==0) boardRepresentation += " ";
        boardRepresentation += "| ";
        boardRepresentation += this.getTurn().toString();
        boardRepresentation += " | ";
        boardRepresentation+= this.getRiver().toString();
        boardRepresentation += " |";
        return boardRepresentation;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public boolean isFull() {
        return this.size() == 5;
    }

    public Board copy() {
        Board board = new Board();
        for(Card c : this){
            board.add(new Card(c));
        }
        return board;
    }

    public boolean isValid() {
        return this.stream().anyMatch(Card::isValid);
    }

    public boolean containsSameRankOf(Card c) {
        for(Card card : this){
            if(card.getRank().equals(c.getRank())) return true;
        }
        return false;
    }
}
