import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;

import java.io.FileReader;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class DB {
    public final CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator("\n");
//    static Driver driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic("neo4j", "neo4j"));
//    public Session session = driver.session();
//    public Transaction tx = session.beginTransaction();
    public Map<FullHand, Integer> rankingPerFullHand = new LinkedHashMap<>();
    public Map<Hand, Double> rankingPerHand = new LinkedHashMap<>();

    public DB (){
        try {
            FileReader pointsReader = new FileReader("CSV/FullHandsOrdered.csv");
            CSVParser pointsParser = new CSVParser(pointsReader, csvFileFormat);
            pointsParser.getRecords().forEach(strings -> {
                rankingPerFullHand.put(new FullHand(strings.get(0)), Integer.valueOf(strings.get(1)));
            });
            pointsReader.close();
            pointsParser.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
        try {
            FileReader pointsReader = new FileReader("CSV/HandsOrdered.csv");
            CSVParser pointsParser = new CSVParser(pointsReader, csvFileFormat);
            pointsParser.getRecords().forEach(strings -> {
                rankingPerHand.put(new Hand(strings.get(0)), Double.valueOf(strings.get(1)));
            });
            pointsReader.close();
            pointsParser.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public List<FullHand> getBestAmongFullHands(List<FullHand> fhs) {
        return fhs.stream().collect(maxList(Comparator.comparingInt(x -> rankingPerFullHand.get(x))));
    }

    static <T> Collector<T,?,List<T>> maxList(Comparator<? super T> comp) {
        return Collector.of(
                ArrayList::new,
                (list, t) -> {
                    int c;
                    if (list.isEmpty() || (c = comp.compare(t, list.get(0))) == 0) {
                        list.add(t);
                    } else if (c > 0) {
                        list.clear();
                        list.add(t);
                    }
                },
                (list1, list2) -> {
                    if (list1.isEmpty()) {
                        return list2;
                    }
                    if (list2.isEmpty()) {
                        return list1;
                    }
                    int r = comp.compare(list1.get(0), list2.get(0));
                    if (r < 0) {
                        return list2;
                    } else if (r > 0) {
                        return list1;
                    } else {
                        list1.addAll(list2);
                        return list1;
                    }
                });
    }

    public FullHand getAnyBestAmongFullHands(List<FullHand> fhs) {
        Integer best = 0;
        FullHand bestHand = null;
        for(FullHand fh : fhs){
            Integer candidate = rankingPerFullHand.get(fh);
            if(candidate > best) {
                best = candidate;
                bestHand = fh;
            }
        }
        return bestHand;
    }

    public List<Hand> getHandsAbove(double threshold) {
        return rankingPerHand.entrySet().stream().filter(handDoubleEntry -> handDoubleEntry.getValue()>threshold).map(Map.Entry::getKey).collect(Collectors.toList());
    }

    public List<Hand> getHandsBelow(double threshold) {
        return rankingPerHand.entrySet().stream().filter(handDoubleEntry -> handDoubleEntry.getValue()<threshold).map(Map.Entry::getKey).collect(Collectors.toList());
    }
}
