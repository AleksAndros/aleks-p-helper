import java.awt.image.BufferedImage; /**
 * Created by Alessandro on 20/09/2017.
 */
public class Card {
    private Rank rank;
    private Suit suit;

    public Card(Rank rank, Suit suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Card() {}

    public Card(Card card) {
        this.rank = card.rank;
        this.suit = card.suit;
    }

    public Card(String s) {
        s = s.toUpperCase();
        switch(s.charAt(0)){
            case 'A':
                this.setRank(Rank.A);
                break;
            case 'K':
                this.setRank(Rank.K);
                break;
            case 'Q':
                this.setRank(Rank.Q);
                break;
            case 'J':
                this.setRank(Rank.J);
                break;
            case 'T':
                this.setRank(Rank.T);
                break;
            case '9':
                this.setRank(Rank.NINE);
                break;
            case '8':
                this.setRank(Rank.EIGHT);
                break;
            case '7':
                this.setRank(Rank.SEVEN);
                break;
            case '6':
                this.setRank(Rank.SIX);
                break;
            case '5':
                this.setRank(Rank.FIVE);
                break;
            case '4':
                this.setRank(Rank.FOUR);
                break;
            case '3':
                this.setRank(Rank.THREE);
                break;
            case '2':
                this.setRank(Rank.TWO);
                break;
        }
        switch(s.charAt(1)){
            case 'S': case '\u2660':
                this.setSuit(Suit.SPADES);
                break;
            case 'H': case '\u2665':
                this.setSuit(Suit.HEARTS);
                break;
            case 'D': case '\u2666':
                this.setSuit(Suit.DIAMONDS);
                break;
            case 'C': case '\u2663':
                this.setSuit(Suit.CLUBS);
                break;
        }

    }

    public Card(BufferedImage rank, BufferedImage suit) {
        this.rank = Rank.getRankFromImage(rank);
        this.suit = Suit.getSuitFromImage(suit);
    }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public void setSuit(Suit suit) {
        this.suit = suit;
    }

    public boolean isSuit(Suit suit){
        return this.suit.equals(suit);
    }

    @Override
    public String toString() {
        return (rank!=null ? rank.toString() : "") + (suit!=null ? suit.toString() : "");
    }

    @Override
    public int hashCode() {
        int hash = this.rank.getRank()-2;
        switch (this.suit){
            case SPADES:
                break;
            case HEARTS:
                hash += 13;
                break;
            case DIAMONDS:
                hash += 13*2;
                break;
            case CLUBS:
                hash += 13*3;
                break;
        }
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Card card = (Card) o;

        return this.rank == card.rank && this.suit == card.suit;
    }

    public String dbRep() {
        return (rank!=null ? rank.toString() : "") + (suit!=null ? suit.dbRep() : "");
    }

    public boolean isValid() {
        return this.rank != null && this.suit != null;
    }
}
