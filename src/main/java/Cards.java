import java.util.*;
import java.util.stream.Collectors;

public class Cards extends LinkedHashSet<Card> {

    public Cards (){
        super();
    }

    public Cards (HashSet<Card> cards){
        super(cards);
    }

    public Cards (Cards cards){
        super(cards);
    }

    public Cards(Set<Card> cards) {
        super(cards);
    }

    public Cards(List<Card> cards) {
        super(cards);
    }

    public Cards(String rep) {
        for(int i = 0; i < rep.length()/2; i++){
            this.add(new Card(rep.substring(2*i, 2*i+2)));
        }
    }

    public Cards(Card firstCard, Card secondCard) {
        this.add(firstCard);
        this.add(secondCard);
    }

    ArrayList<Card> sortedByRank() {
        ArrayList<Card> sorted = new ArrayList<>();
        sorted.addAll(this);
        sorted.sort(Comparator.comparingInt(a -> a.getRank().getRank()));
        Collections.reverse(sorted);
        return sorted;
    }

    Rank getRankOfMostCards(){
        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));

        Optional<List<Card>> optMostCards = cardsByRanking.values().stream().max( (a, b) -> {
            if(a.size() > b.size()) return 1;
            else if(a.size() < b.size()) return -1;
            else return Integer.compare(a.get(0).getRank().getRank(), b.get(0).getRank().getRank());
        });

        List<Card> mostCards = null;
        if(optMostCards.isPresent()){
            mostCards = optMostCards.get();
        }
        assert mostCards != null;
        return mostCards.get(0).getRank();
    }

    @Override
    public int hashCode() {
        int hash = 1;
        ArrayList<Card> ordered = new ArrayList<>(this);
        ordered.sort(Comparator.comparingInt(Card::hashCode));
        Collections.reverse(ordered);
        for(Card c : ordered){
            hash = hash*Primes.getNthPrime(c.hashCode());
        }
        return hash;
    }

    public String dbRep() {
        ArrayList<Card> ordered = new ArrayList<>(this);
        ordered.sort(Comparator.comparingInt(Card::hashCode));
        Collections.reverse(ordered);
        StringBuilder rep = new StringBuilder();
        for(Card c : ordered){
            rep.append(c.dbRep());
        }
        return rep.toString();
    }
}
