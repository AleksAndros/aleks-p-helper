public enum Round {
    PREFLOP,
    FLOP,
    TURN,
    RIVER;

    public Round nextRound() {
        switch (this){
            case PREFLOP:
                return FLOP;
            case FLOP:
                return TURN;
            case TURN:
                return RIVER;
            default:
                return null;
        }
    }
}
