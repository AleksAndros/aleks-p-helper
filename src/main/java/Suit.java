import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro on 20/09/2017.
 */
public enum Suit {
    SPADES ("S"),
    HEARTS ("H"),
    DIAMONDS ("D"),
    CLUBS ("C");

    private List<BufferedImage> associatedImages = new ArrayList<>();

    private Suit(String fileName){
        for(int i = 1; i<8; i++){
            File f = new File("cardsImages/" + fileName + i + ".png");
            try {
                this.associatedImages.add(ImageIO.read(f));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        switch (this){
            case SPADES:
                return "\u2660";
            case HEARTS:
                return "\u2665";
            case DIAMONDS:
                return "\u2666";
            case CLUBS:
                return "\u2663";
            default:
                return "";
        }
    }

    public String dbRep() {
        switch (this){
            case SPADES:
                return "S";
            case HEARTS:
                return "H";
            case DIAMONDS:
                return "D";
            case CLUBS:
                return "C";
            default:
                return "";
        }
    }

    public List<BufferedImage> getAssociatedImages() {
        return associatedImages;
    }

    public static Suit getSuitFromImage(BufferedImage img) {
        for(Suit s : Suit.values()){
            for(BufferedImage i : s.associatedImages){
                if(ImageHandler.compareImages(i, img)) return s;
            }
        }
        return null;
    }
}
