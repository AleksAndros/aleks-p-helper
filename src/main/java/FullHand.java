import java.util.*;
import java.util.stream.Collectors;

public class FullHand extends Cards {

    public FullHand (FullHand fullHand){
        this.addAll(fullHand);
    }

    public FullHand (HashSet<Card> cards){
        super(cards);
    }

    public FullHand (Cards cards){
        super(cards);
    }

    public FullHand(Set<Card> cards) {
        super(cards);
    }

    public FullHand(List<Card> cards) {
        super(cards);
    }

    public FullHand(String rep) {
        super(rep);
    }

    public Point getPoint() {

        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));
        ArrayList<Card> sortedCards = this.sortedByRank();
        Rank rankOfMostCards = this.getRankOfMostCards();
        Rank strongestRank = sortedCards.get(0).getRank();

        boolean allSameSuit = this.allSameSuit(); // Royal, Flush
        boolean straight = this.isStraight(); // Royal, Straight
        if(straight) strongestRank = this.getStrongestRankOfStraight();

        if(allSameSuit && straight) {
            return new Point(Ranking.STRAIGHT_FLUSH, strongestRank);
        }
        else if(this.isPoker(cardsByRanking)){
            sortedCards.removeAll(cardsByRanking.get(rankOfMostCards));
            return new Point(Ranking.POKER, rankOfMostCards, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else if(this.isFullHouse(cardsByRanking)) {
            sortedCards.removeAll(cardsByRanking.get(rankOfMostCards));
            return new Point(Ranking.FULL_HOUSE, rankOfMostCards, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else if(allSameSuit) {
            sortedCards.removeAll(cardsByRanking.get(strongestRank));
            return new Point(Ranking.FLUSH, strongestRank, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else if(straight) {
            return new Point(Ranking.STRAIGHT, strongestRank);
        }
        else if(this.isThreeOfAKind(cardsByRanking)){
            sortedCards.removeAll(cardsByRanking.get(rankOfMostCards));
            return new Point(Ranking.THREE_OF_A_KIND, rankOfMostCards, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else if(this.isTwoPair(cardsByRanking)){
            sortedCards.removeAll(cardsByRanking.get(rankOfMostCards));
            return new Point(Ranking.TWO_PAIR, rankOfMostCards, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else if(this.isPair(cardsByRanking)) {
            sortedCards.removeAll(cardsByRanking.get(rankOfMostCards));
            return new Point(Ranking.PAIR, rankOfMostCards, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
        else {
            sortedCards.removeAll(cardsByRanking.get(strongestRank));
            return new Point(Ranking.HIGH_CARD, strongestRank, sortedCards.stream().map(Card::getRank).collect(Collectors.toList()));
        }
//        return null;
    }

    private boolean isTwoPair(Map<Rank, List<Card>> cards) {
        boolean firstPair = false;
        boolean secondPair = false;
        for ( Map.Entry<Rank, List<Card>> entry : cards.entrySet() ){
            if(entry.getValue().size() == 2){
                if(!firstPair) firstPair = true;
                else if(!secondPair) secondPair = true;
            }
        }
        return firstPair && secondPair;

    }

    private boolean isFullHouse(Map<Rank, List<Card>> cards) {
        return this.isThreeOfAKind(cards) && this.isPair(cards);
    }

    private boolean isThreeOfAKind(Map<Rank, List<Card>> cards) {
        for ( Map.Entry<Rank, List<Card>> entry : cards.entrySet() ){
            if(entry.getValue().size() == 3) return true;
        }
        return false;
    }

    private boolean isPair(Map<Rank, List<Card>> cards) {
        for ( Map.Entry<Rank, List<Card>> entry : cards.entrySet() ){
            if(entry.getValue().size() == 2) return true;
        }
        return false;
    }

    private boolean isPoker(Map<Rank, List<Card>> cards) {
        for ( Map.Entry<Rank, List<Card>> entry : cards.entrySet() ){
            if(entry.getValue().size() == 4) return true;
        }
        return false;
    }

    private boolean isFullHouse() {
        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));
        return this.isFullHouse(cardsByRanking);
    }

    private boolean isThreeOfAKind() {
        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));
        return this.isThreeOfAKind(cardsByRanking);
    }

    private boolean isPair() {
        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));
        return this.isPair(cardsByRanking);
    }

    private boolean isPoker() {
        Map<Rank, List<Card>> cardsByRanking = this.stream().collect(Collectors.groupingBy(Card::getRank));
        return this.isPoker(cardsByRanking);
    }

    private boolean isStraight() {
        List<Integer> ranksOrdered = this.sortedByRank().stream().map(Card::getRank).map(Rank::getRank).collect(Collectors.toList());

        for(int i=0; i<4; i++){
            if(!ranksOrdered.get(i).equals(ranksOrdered.get(i+1)+1)){ // If c_{i+1} != c_{i}+1
                if(i!=0) return false;
                else {
                    if(ranksOrdered.get(i).equals(Rank.A.getRank())){
                        if(!ranksOrdered.get(i+1).equals(Rank.FIVE.getRank()))
                            return false;
                    }
                    else return false;
                }
            }
        }

        return true;
    }

    private Rank getStrongestRankOfStraight() {
        List<Rank> ranksOrdered = this.sortedByRank().stream().map(Card::getRank).collect(Collectors.toList());
        if(ranksOrdered.get(0).equals(Rank.A) && ranksOrdered.get(1).equals(Rank.FIVE) ){ // Limit case: A 5 4 3 2
            return Rank.FIVE;
        }
        else return ranksOrdered.get(0);
    }

    private boolean allSameSuit(){
        for(Suit s : Suit.values()){
            if(this.stream().allMatch(card -> card.isSuit(s))){
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
//        return super.toString();
        StringBuilder fullHandRepresentation = new StringBuilder();
        this.forEach(card -> fullHandRepresentation.append(card.toString()).append(" "));
        return fullHandRepresentation.toString();
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public boolean isCompatibleWith(Hand hand, Board board) {
        int numberOfCardsNotInBoard = 0;
        for(Card c : this){
            if(hand.contains(c)) return false;
            if(!board.contains(c)) numberOfCardsNotInBoard++;
            if(numberOfCardsNotInBoard > 2) return false;
        }
        return true;
    }
}
