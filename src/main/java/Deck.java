import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by Alessandro on 20/09/2017.
 */
public class Deck extends ArrayList<Card> {

    public Deck (){
        for (Suit suit: Suit.values()) {
            for (Rank rank: Rank.values()){
                this.add(new Card(rank, suit));
            }
        }
    }

    public Deck(Deck deck) {
        super(deck);
    }

    public Card takeCard(){
        int randomNum = ThreadLocalRandom.current().nextInt(0, this.size()-1);
        Card c = this.get(randomNum);
        this.remove(c);
        return c;
    }

    public Cards takeCards (int numberOfCards){
        Cards cards = new Cards();
        for(int i=0; i<numberOfCards; i++){
            cards.add(this.takeCard());
        }
        return cards;
    }

    @Override
    public boolean add(Card card) {
        return !this.contains(card) && super.add(card);
    }

    @Override
    public boolean addAll(Collection<? extends Card> cards) {
        boolean addedSomething = false;
        for(Card c : cards){
            if(this.add(c)) addedSomething = true;
        }
        return addedSomething;
    }
}
