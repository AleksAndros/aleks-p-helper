import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RandomGame extends Game {

    public RandomGame(int numberOfPlayers){
        Deck d = new Deck();
        List<Player> players = new ArrayList<>();
        while(players.size() < numberOfPlayers) {
            players.add(new Player(new Hand(d.takeCards(2))));
        }

        this.board = new Board();
        this.players = players;
        for (Player player : this.players) {
            player.setGame(this);
        }
        this.setAppropriateRound();
    }

    public RandomGame(){
        this(ThreadLocalRandom.current().nextInt(2, 9));
    }
}
