import java.util.ArrayList;
import java.util.Arrays;

public final class Primes extends ArrayList<Integer> {

    private static final Primes primes = new Primes(500);

    private Primes(int n){
        boolean [] numbers = new boolean[n];
        Arrays.fill(numbers, true);
        for(int i=2; i<=Math.sqrt(n); i++){
            if(numbers[i]){
                for(int j=i*i; j<n; j+=i){
                    numbers[j] = false;
                }
            }
        }
        for(int i=2; i<n; i++){
            if(numbers[i]) this.add(i);
        }
    }

    public static Integer getNthPrime(Integer n) {
        return primes.get(n);
    }

    public static Primes getPrimes() {
        return primes;
    }
}
