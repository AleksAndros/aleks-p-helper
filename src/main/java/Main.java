import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.paukov.combinatorics3.Generator;
import umontreal.iro.lecuyer.util.Chrono;

import java.io.*;
import java.util.Map;

public class Main {

    static DB db;

    public static void main(String[] args) throws IOException {

//        Main.train();

//        Main.live(Boolean.parseBoolean(args[0]), Integer.parseInt(args[1]));

        Main.simulate();

//        Chrono timer2 = new Chrono();

//        Main.estimation(9);
//        Main.fixedGame();
//        Main.randomGames(9,10);

//        Main.live(true,6);
//        Main.live(true,9);
//        Main.live(false,6);
//        Main.live(false,9);

//        System.out.println("Total CPU time: " + timer2.format());
    }

    public static void train(){
        db = new DB();
        System.out.println("Finished loading the DB!");
        Game game = new RandomGame(3);
        System.out.println(game);
        game.play();
        System.out.println(game);
    }

    public static void simulate(){
        db = new DB();
        System.out.println("Finished loading the DB!");
        ImageHandler imageHandler = new ImageHandler();
        imageHandler.getScreenshotFromScreenshotNumber(171);
        imageHandler.acquireInfos(false,6);
        Game game = new Game(imageHandler);
        System.out.println(game);
        Chrono timer = new Chrono();
//        System.out.println("Timer started");
        for(Player p : game.getPlayers()){
            if(game.getPlayers().indexOf(p) == 0) {
                System.out.println(game.getProbabilitiesFor(p));
                continue;
            }
            System.out.println(p.getName() + "\t" + p.getCurrentBet() + " / " + p.getStack()); // + "\t" + p.getHandDistribution().size());
        }
        System.out.println("Total CPU time: " + timer.format());

        imageHandler.closeDevice();
    }

    public static void takeScreenshot(){
        db = new DB();
        ImageHandler imageHandler = new ImageHandler();
        imageHandler.getScreenshotFromScreenshotNumber(210);
        imageHandler.acquireInfos(true,9);
        System.out.println(new Game(imageHandler));
        imageHandler.saveScreenshot();
        imageHandler.closeDevice();
    }

    public static void live(boolean onLeft, int numberOfTotalPlayers) {

        db = new DB();
        ImageHandler imageHandler = new ImageHandler();
        Game game = null;

        while(true) {
            System.out.println("Ready.");
            try {
                BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
                bufferRead.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Processing...");

            imageHandler.getScreenshot();
            imageHandler.acquireInfos(onLeft,numberOfTotalPlayers);
//            imageHandler.saveScreenshot();

            if(game == null) game = new Game(imageHandler);
            else game.updateWith(imageHandler);
            game = new Game(imageHandler);
            System.out.println(game);
            if(!game.isValid()) {
                continue;
            }
            System.out.println(game.getProbabilitiesFor(game.getPlayers().get(0)));
        }
    }

//    public static void fixedGame(){
//        db = new DB();
//        Board board = new Board(
//                new Card("Js"),
//                new Card("Qd"),
//                new Card("9h"),
//                new Card("5s")
//        );
//
//        Player alessandro = new Player("Alessandro", new Hand(
//                new Card("As"),
//                new Card("Kd")
//        ));
//
//        Player tiziano = new Player("Tiziano", new Hand(
//                new Card("7s"),
//                new Card("6s")
//        ));
//
////        Player ilaria = new Player("Ilaria", new Hand(
////                new Card(Rank.EIGHT, Suit.DIAMONDS),
////                new Card(Rank.K, Suit.HEARTS)
////        ));
//
//        ArrayList<Player> players = new ArrayList<>();
//        players.add(alessandro);
//        players.add(tiziano);
////        players.add(ilaria);
//
//        Game game = new Game(players, board, players.size());
//
////        game.setDealer();
//        game.setPot(600);
//        game.setBigBlind(60);
//
//        System.out.println(game.toString());
//
////        while(!game.isFinished()){
////            game.getPlayers().get(game.getPosition()).doWhatHeShouldDo();
////        }
//
//    }

    public static void estimation(int numberOfPlayers){
        db = new DB();
        Player alessandro = new Player("Ale", new Hand(
                new Card(Rank.A, Suit.SPADES),
                new Card(Rank.A, Suit.DIAMONDS)
        ));

//        Board board = new Board();
//        Game game = new Game(alessandro, board, numberOfPlayers);
//        System.out.println(game.toString());
//        System.out.println("Probabilities: \n" + game.getProbabilitiesFor(alessandro) + "\n\n\n");


        Board board = new Board(new Card("KS"), new Card("TS"), new Card("KH"));
        Game game = new Game(alessandro, board, 4);
        game.setPot(600);
        System.out.println(game.toString());
        System.out.println("Probabilities: \n" + game.getProbabilitiesFor(alessandro) + "\n\n\n");
        System.out.println("Advanced Probabilities: \n" + game.getProbabilitiesFor(alessandro) + "\n\n\n");
//        System.out.println(game.getHandsThatCanCallTo(2500));
    }

    private static void printEquities(Map<Integer, Probabilities> map) {
        System.out.println();

        System.out.format("%20s","Number of players | ");
        map.forEach((integer, probabilities) -> System.out.format("%10d", integer));
        System.out.println();

        System.out.format("%20s","Win | ");
        map.forEach((integer, probabilities) -> System.out.format("%10s", String.format("%.2f",probabilities.getWinning()*100) + "%"));
        System.out.println();

        System.out.format("%20s","Tie | ");
        map.forEach((integer, probabilities) -> System.out.format("%10s", String.format("%.2f",probabilities.getTieing()*100) + "%"));
        System.out.println();

        System.out.format("%20s","Lose | ");
        map.forEach((integer, probabilities) -> System.out.format("%10s", String.format("%.2f",probabilities.getLosing()*100) + "%"));
        System.out.println();
    }
}
