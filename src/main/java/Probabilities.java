public class Probabilities {
    private double winning;
    private double tieing;
    private double losing;
    private int sample = 0;

    public Probabilities (double winning, double tieing, double losing){
        this.winning = winning;
        this.tieing = tieing;
        this.losing = losing;
    }

    public double getWinning() {
        return winning;
    }

    public double getTieing() {
        return tieing;
    }

    public double getLosing() {
        return losing;
    }

    public void setWinning(Float winning) {
        this.winning = winning;
    }

    public void setTieing(Float tieing) {
        this.tieing = tieing;
    }

    public void setLosing(Float losing) {
        this.losing = losing;
    }

    public void addSimulation(Probabilities p){
        this.winning = (this.winning*this.sample + p.winning)/(this.sample+1);
        this.tieing = (this.tieing*this.sample + p.tieing)/(this.sample+1);
        this.losing = (this.losing*this.sample + p.losing)/(this.sample+1);
        this.sample++;
    }

    @Override
    public String toString() {
        return "Win: " + String.format("%.2f",this.winning*100) + "%\nTie: " + String.format("%.2f",this.tieing*100) + "%\nLose: " + String.format("%.2f",this.losing*100) + "%";
    }
}
