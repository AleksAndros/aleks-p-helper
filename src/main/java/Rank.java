import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alessandro on 20/09/2017.
 */
public enum Rank {
    A(14),
    K(13),
    Q(12),
    J(11),
    T(10),
    NINE(9),
    EIGHT(8),
    SEVEN(7),
    SIX(6),
    FIVE(5),
    FOUR(4),
    THREE(3),
    TWO(2);

    private Integer rank;
    private List<BufferedImage> associatedImages = new ArrayList<>();

    Rank(Integer rank) {
        this.rank = rank;
    }

    public Integer getRank() {
        return rank;
    }

    @Override
    public String toString() {
        switch (this){
            case A:
                return "A";
            case K:
                return "K";
            case Q:
                return "Q";
            case J:
                return "J";
            case T:
                return "T";
            default:
                return this.rank.toString();
        }
    }

    public static Rank getRankFromChar(char c) {
        switch (c){
            case 'A':
                return A;
            case 'K':
                return K;
            case 'Q':
                return Q;
            case 'J':
                return J;
            case 'T':
                return T;
            case '9':
                return NINE;
            case '8':
                return EIGHT;
            case '7':
                return SEVEN;
            case '6':
                return SIX;
            case '5':
                return FIVE;
            case '4':
                return FOUR;
            case '3':
                return THREE;
            case '2':
                return TWO;
            default:
                return null;
        }
    }

    public static Rank getRankFromImage(BufferedImage img) {
        for(Rank r : Rank.values()){
            if(r.associatedImages.isEmpty()){
                for(int i = 1; i<8; i++) {
                    File f = new File("cardsImages/" + r.toString() + "B" + i + ".png");
                    try {
                        r.associatedImages.add(ImageIO.read(f));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    f = new File("cardsImages/" + r.toString() + "R" + i + ".png");
                    try {
                        r.associatedImages.add(ImageIO.read(f));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            for(BufferedImage i : r.associatedImages){
                if(ImageHandler.compareImages(i, img)) return r;
            }
        }
        return null;
    }
}
