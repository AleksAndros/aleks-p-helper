import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.RawImage;
import com.android.ddmlib.TimeoutException;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImageHandler {

    private IDevice device;
    private ITesseract tesseract = new Tesseract();

    private BufferedImage image;
    private BufferedImage firstCardRankImage;
    private BufferedImage firstCardSuitImage;
    private BufferedImage secondCardRankImage;
    private BufferedImage secondCardSuitImage;
    private Hand hand;
    private Board board;
    private int pot;
    private int bigBlind;
    private List<Player> players = new ArrayList<>();

    public ImageHandler() {
        AndroidDebugBridge.init(false /* debugger support */);
        AndroidDebugBridge bridge = AndroidDebugBridge.createBridge("adb", true /* forceNewBridge */);

        int count = 0;
        while (!bridge.hasInitialDeviceList()) {
            try {
                Thread.sleep(100);
                count++;
            } catch (InterruptedException e) {
                // pass
            }

            // let's not wait > 10 sec.
            if (count > 100) {
                System.err.println("Timeout getting device list!");
                return;
            }
        }

        IDevice[] devices = bridge.getDevices();
        for (IDevice d : devices) {
            if (!d.isEmulator()) device = d;
        }

        tesseract.setDatapath(LoadLibs.extractTessResources("tessdata").getParent());
        tesseract.setTessVariable("tessedit_char_whitelist","0123456789abcdefghilmnopqrstuvzABCDEFGHILMNOPQRSTUVZ.");
//        tesseract.setTessVariable("tessedit_char_whitelist", "0123456789./");
    }

    public void closeDevice() {
        AndroidDebugBridge.terminate();
    }

    public void getScreenshot() {
        RawImage rawImage;
        try {
            rawImage = device.getScreenshot();
        } catch (TimeoutException e) {
            System.out.println("Unable to get frame buffer: timeout");
            return;
        } catch (Exception ioe) {
            System.out.println("Unable to get frame buffer: " + ioe.getMessage());
            return;
        }
//        rawImage = rawImage.getRotated();
        BufferedImage image = new BufferedImage(rawImage.width, rawImage.height,
                BufferedImage.TYPE_INT_ARGB);

        int index = 0;
        int IndexInc = rawImage.bpp >> 3;
        for (int y = 0; y < rawImage.height; y++) {
            for (int x = 0; x < rawImage.width; x++) {
                int value = rawImage.getARGB(index);
                index += IndexInc;
                image.setRGB(x, y, value);
            }
        }

        this.image = image;
    }

    public void saveScreenshot() {
        int count = 212;
        boolean found = false;

        while (!found) {
            try {
                ImageIO.read(new File("screenshots/phoneScreenshot" + count + ".png"));
                count++;
            } catch (IOException e) {
                found = true;
            }
        }
//
//        try {
//            ImageIO.write(image, "png", new File("screenshots/phoneScreenshot" + count + ".png"));
//            saveInfos(count);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

//    public void saveInfos(int i){
//        try {
//            ImageIO.write(this.firstCardRankImage, "png", new File("screenshots/parts/firstCardRankImage" + i + ".png"));
//            ImageIO.write(this.secondCardRankImage, "png", new File("screenshots/parts/secondCardRankImage" + i + ".png"));
//            ImageIO.write(this.firstCardSuitImage, "png", new File("screenshots/parts/firstCardSuitImage" + i + ".png"));
//            ImageIO.write(this.secondCardSuitImage, "png", new File("screenshots/parts/secondCardSuitImage" + i + ".png"));
////            ImageIO.write(this.firstCardOnBoardRankImage, "png", new File("screenshots/parts/firstCardOnBoardRankImage" + i + ".png"));
////            ImageIO.write(this.secondCardOnBoardRankImage, "png", new File("screenshots/parts/secondCardOnBoardRankImage" + i + ".png"));
////            ImageIO.write(this.thirdCardOnBoardRankImage, "png", new File("screenshots/parts/thirdCardOnBoardRankImage" + i + ".png"));
////            ImageIO.write(this.fourthCardOnBoardRankImage, "png", new File("screenshots/parts/fourthCardOnBoardRankImage" + i + ".png"));
////            ImageIO.write(this.fifthCardOnBoardRankImage, "png", new File("screenshots/parts/fifthCardOnBoardRankImage" + i + ".png"));
////            ImageIO.write(this.firstCardOnBoardSuitImage, "png", new File("screenshots/parts/firstCardOnBoardSuitImage" + i + ".png"));
////            ImageIO.write(this.secondCardOnBoardSuitImage, "png", new File("screenshots/parts/secondCardOnBoardSuitImage" + i + ".png"));
////            ImageIO.write(this.thirdCardOnBoardSuitImage, "png", new File("screenshots/parts/thirdCardOnBoardSuitImage" + i + ".png"));
////            ImageIO.write(this.fourthCardOnBoardSuitImage, "png", new File("screenshots/parts/fourthCardOnBoardSuitImage" + i + ".png"));
////            ImageIO.write(this.fifthCardOnBoardSuitImage, "png", new File("screenshots/parts/fifthCardOnBoardSuitImage" + i + ".png"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void acquirePlayers(int numberOfTotalPlayers) {
        this.players = new ArrayList<>();
        for (int i = 0; i < numberOfTotalPlayers-1; i++) {
            this.players.add(new Player());
        }

        Color color = new Color(255, 255, 255);
        BufferedImage colorImage = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB);
        colorImage.setRGB(0, 0, color.getRGB());

        if (numberOfTotalPlayers == 6) {
            BufferedImage player1 = image.getSubimage(PixelPosition.SixPlayers1.getX(), PixelPosition.SixPlayers1.getY(), 1, 1);
            BufferedImage player2 = image.getSubimage(PixelPosition.SixPlayers2.getX(), PixelPosition.SixPlayers2.getY(), 1, 1);
            BufferedImage player3 = image.getSubimage(PixelPosition.SixPlayers3.getX(), PixelPosition.SixPlayers3.getY(), 1, 1);
            BufferedImage player4 = image.getSubimage(PixelPosition.SixPlayers4.getX(), PixelPosition.SixPlayers4.getY(), 1, 1);
            BufferedImage player5 = image.getSubimage(PixelPosition.SixPlayers5.getX(), PixelPosition.SixPlayers5.getY(), 1, 1);
            double d1 = getRGBDistance(player1, colorImage, 0, 0);
            double d2 = getRGBDistance(player2, colorImage, 0, 0);
            double d3 = getRGBDistance(player3, colorImage, 0, 0);
            double d4 = getRGBDistance(player4, colorImage, 0, 0);
            double d5 = getRGBDistance(player5, colorImage, 0, 0);

            if (d1 < 20) this.players.get(0).setActive();
            if (d2 < 20) this.players.get(1).setActive();
            if (d3 < 20) this.players.get(2).setActive();
            if (d4 < 20) this.players.get(3).setActive();
            if (d5 < 20) this.players.get(4).setActive();
            
            BufferedImage player1CurrentBet = image.getSubimage(PixelPosition.SixPlayers1CurrentBet.getX(), PixelPosition.SixPlayers1CurrentBet.getY(), PixelPosition.SixPlayers1CurrentBet.getWidth(), PixelPosition.SixPlayers1CurrentBet.getHeight());
            this.players.get(0).setCurrentBet(readNumberThroughOCR(player1CurrentBet));
            BufferedImage player2CurrentBet = image.getSubimage(PixelPosition.SixPlayers2CurrentBet.getX(), PixelPosition.SixPlayers2CurrentBet.getY(), PixelPosition.SixPlayers2CurrentBet.getWidth(), PixelPosition.SixPlayers2CurrentBet.getHeight());
            this.players.get(1).setCurrentBet(readNumberThroughOCR(player2CurrentBet));
            BufferedImage player3CurrentBet = image.getSubimage(PixelPosition.SixPlayers3CurrentBet.getX(), PixelPosition.SixPlayers3CurrentBet.getY(), PixelPosition.SixPlayers3CurrentBet.getWidth(), PixelPosition.SixPlayers3CurrentBet.getHeight());
            this.players.get(2).setCurrentBet(readNumberThroughOCR(player3CurrentBet));
            BufferedImage player4CurrentBet = image.getSubimage(PixelPosition.SixPlayers4CurrentBet.getX(), PixelPosition.SixPlayers4CurrentBet.getY(), PixelPosition.SixPlayers4CurrentBet.getWidth(), PixelPosition.SixPlayers4CurrentBet.getHeight());
            this.players.get(3).setCurrentBet(readNumberThroughOCR(player4CurrentBet));
            BufferedImage player5CurrentBet = image.getSubimage(PixelPosition.SixPlayers5CurrentBet.getX(), PixelPosition.SixPlayers5CurrentBet.getY(), PixelPosition.SixPlayers5CurrentBet.getWidth(), PixelPosition.SixPlayers5CurrentBet.getHeight());
            this.players.get(4).setCurrentBet(readNumberThroughOCR(player5CurrentBet));

            BufferedImage player1Stack = image.getSubimage(PixelPosition.SixPlayers1Stack.getX(), PixelPosition.SixPlayers1Stack.getY(), PixelPosition.SixPlayers1Stack.getWidth(), PixelPosition.SixPlayers1Stack.getHeight());
            this.players.get(0).setStack(readNumberThroughOCR(player1Stack));
            BufferedImage player2Stack = image.getSubimage(PixelPosition.SixPlayers2Stack.getX(), PixelPosition.SixPlayers2Stack.getY(), PixelPosition.SixPlayers2Stack.getWidth(), PixelPosition.SixPlayers2Stack.getHeight());
            this.players.get(1).setStack(readNumberThroughOCR(player2Stack));
            BufferedImage player3Stack = image.getSubimage(PixelPosition.SixPlayers3Stack.getX(), PixelPosition.SixPlayers3Stack.getY(), PixelPosition.SixPlayers3Stack.getWidth(), PixelPosition.SixPlayers3Stack.getHeight());
            this.players.get(2).setStack(readNumberThroughOCR(player3Stack));
            BufferedImage player4Stack = image.getSubimage(PixelPosition.SixPlayers4Stack.getX(), PixelPosition.SixPlayers4Stack.getY(), PixelPosition.SixPlayers4Stack.getWidth(), PixelPosition.SixPlayers4Stack.getHeight());
            this.players.get(3).setStack(readNumberThroughOCR(player4Stack));
            BufferedImage player5Stack = image.getSubimage(PixelPosition.SixPlayers5Stack.getX(), PixelPosition.SixPlayers5Stack.getY(), PixelPosition.SixPlayers5Stack.getWidth(), PixelPosition.SixPlayers5Stack.getHeight());
            this.players.get(4).setStack(readNumberThroughOCR(player5Stack));
        }
        if (numberOfTotalPlayers == 9) {
            BufferedImage player1 = image.getSubimage(PixelPosition.NinePlayers1.getX(), PixelPosition.NinePlayers1.getY(), 1, 1);
            BufferedImage player2 = image.getSubimage(PixelPosition.NinePlayers2.getX(), PixelPosition.NinePlayers2.getY(), 1, 1);
            BufferedImage player3 = image.getSubimage(PixelPosition.NinePlayers3.getX(), PixelPosition.NinePlayers3.getY(), 1, 1);
            BufferedImage player4 = image.getSubimage(PixelPosition.NinePlayers4.getX(), PixelPosition.NinePlayers4.getY(), 1, 1);
            BufferedImage player5 = image.getSubimage(PixelPosition.NinePlayers5.getX(), PixelPosition.NinePlayers5.getY(), 1, 1);
            BufferedImage player6 = image.getSubimage(PixelPosition.NinePlayers6.getX(), PixelPosition.NinePlayers6.getY(), 1, 1);
            BufferedImage player7 = image.getSubimage(PixelPosition.NinePlayers7.getX(), PixelPosition.NinePlayers7.getY(), 1, 1);
            BufferedImage player8 = image.getSubimage(PixelPosition.NinePlayers8.getX(), PixelPosition.NinePlayers8.getY(), 1, 1);
            double d1 = getRGBDistance(player1, colorImage, 0, 0);
            double d2 = getRGBDistance(player2, colorImage, 0, 0);
            double d3 = getRGBDistance(player3, colorImage, 0, 0);
            double d4 = getRGBDistance(player4, colorImage, 0, 0);
            double d5 = getRGBDistance(player5, colorImage, 0, 0);
            double d6 = getRGBDistance(player6, colorImage, 0, 0);
            double d7 = getRGBDistance(player7, colorImage, 0, 0);
            double d8 = getRGBDistance(player8, colorImage, 0, 0);
            if (d1 < 20) this.players.get(0).setActive();
            if (d2 < 20) this.players.get(1).setActive();
            if (d3 < 20) this.players.get(2).setActive();
            if (d4 < 20) this.players.get(3).setActive();
            if (d5 < 20) this.players.get(4).setActive();
            if (d6 < 20) this.players.get(5).setActive();
            if (d7 < 20) this.players.get(6).setActive();
            if (d8 < 20) this.players.get(7).setActive();

            BufferedImage player1CurrentBet = image.getSubimage(PixelPosition.NinePlayers1CurrentBet.getX(), PixelPosition.NinePlayers1CurrentBet.getY(), PixelPosition.NinePlayers1CurrentBet.getWidth(), PixelPosition.NinePlayers1CurrentBet.getHeight());
            this.players.get(0).setCurrentBet(readNumberThroughOCR(player1CurrentBet));
            BufferedImage player2CurrentBet = image.getSubimage(PixelPosition.NinePlayers2CurrentBet.getX(), PixelPosition.NinePlayers2CurrentBet.getY(), PixelPosition.NinePlayers2CurrentBet.getWidth(), PixelPosition.NinePlayers2CurrentBet.getHeight());
            this.players.get(1).setCurrentBet(readNumberThroughOCR(player2CurrentBet));
            BufferedImage player3CurrentBet = image.getSubimage(PixelPosition.NinePlayers3CurrentBet.getX(), PixelPosition.NinePlayers3CurrentBet.getY(), PixelPosition.NinePlayers3CurrentBet.getWidth(), PixelPosition.NinePlayers3CurrentBet.getHeight());
            this.players.get(2).setCurrentBet(readNumberThroughOCR(player3CurrentBet));
            BufferedImage player4CurrentBet = image.getSubimage(PixelPosition.NinePlayers4CurrentBet.getX(), PixelPosition.NinePlayers4CurrentBet.getY(), PixelPosition.NinePlayers4CurrentBet.getWidth(), PixelPosition.NinePlayers4CurrentBet.getHeight());
            this.players.get(3).setCurrentBet(readNumberThroughOCR(player4CurrentBet));
            BufferedImage player5CurrentBet = image.getSubimage(PixelPosition.NinePlayers5CurrentBet.getX(), PixelPosition.NinePlayers5CurrentBet.getY(), PixelPosition.NinePlayers5CurrentBet.getWidth(), PixelPosition.NinePlayers5CurrentBet.getHeight());
            this.players.get(4).setCurrentBet(readNumberThroughOCR(player5CurrentBet));
            BufferedImage player6CurrentBet = image.getSubimage(PixelPosition.NinePlayers6CurrentBet.getX(), PixelPosition.NinePlayers6CurrentBet.getY(), PixelPosition.NinePlayers6CurrentBet.getWidth(), PixelPosition.NinePlayers6CurrentBet.getHeight());
            this.players.get(5).setCurrentBet(readNumberThroughOCR(player6CurrentBet));
            BufferedImage player7CurrentBet = image.getSubimage(PixelPosition.NinePlayers7CurrentBet.getX(), PixelPosition.NinePlayers7CurrentBet.getY(), PixelPosition.NinePlayers7CurrentBet.getWidth(), PixelPosition.NinePlayers7CurrentBet.getHeight());
            this.players.get(6).setCurrentBet(readNumberThroughOCR(player7CurrentBet));
            BufferedImage player8CurrentBet = image.getSubimage(PixelPosition.NinePlayers8CurrentBet.getX(), PixelPosition.NinePlayers8CurrentBet.getY(), PixelPosition.NinePlayers8CurrentBet.getWidth(), PixelPosition.NinePlayers8CurrentBet.getHeight());
            this.players.get(7).setCurrentBet(readNumberThroughOCR(player8CurrentBet));

            BufferedImage player1Stack = image.getSubimage(PixelPosition.NinePlayers1Stack.getX(), PixelPosition.NinePlayers1Stack.getY(), PixelPosition.NinePlayers1Stack.getWidth(), PixelPosition.NinePlayers1Stack.getHeight());
            this.players.get(0).setStack(readNumberThroughOCR(player1Stack));
            BufferedImage player2Stack = image.getSubimage(PixelPosition.NinePlayers2Stack.getX(), PixelPosition.NinePlayers2Stack.getY(), PixelPosition.NinePlayers2Stack.getWidth(), PixelPosition.NinePlayers2Stack.getHeight());
            this.players.get(1).setStack(readNumberThroughOCR(player2Stack));
            BufferedImage player3Stack = image.getSubimage(PixelPosition.NinePlayers3Stack.getX(), PixelPosition.NinePlayers3Stack.getY(), PixelPosition.NinePlayers3Stack.getWidth(), PixelPosition.NinePlayers3Stack.getHeight());
            this.players.get(2).setStack(readNumberThroughOCR(player3Stack));
            BufferedImage player4Stack = image.getSubimage(PixelPosition.NinePlayers4Stack.getX(), PixelPosition.NinePlayers4Stack.getY(), PixelPosition.NinePlayers4Stack.getWidth(), PixelPosition.NinePlayers4Stack.getHeight());
            this.players.get(3).setStack(readNumberThroughOCR(player4Stack));
            BufferedImage player5Stack = image.getSubimage(PixelPosition.NinePlayers5Stack.getX(), PixelPosition.NinePlayers5Stack.getY(), PixelPosition.NinePlayers5Stack.getWidth(), PixelPosition.NinePlayers5Stack.getHeight());
            this.players.get(4).setStack(readNumberThroughOCR(player5Stack));
            BufferedImage player6Stack = image.getSubimage(PixelPosition.NinePlayers6Stack.getX(), PixelPosition.NinePlayers6Stack.getY(), PixelPosition.NinePlayers6Stack.getWidth(), PixelPosition.NinePlayers6Stack.getHeight());
            this.players.get(5).setStack(readNumberThroughOCR(player6Stack));
            BufferedImage player7Stack = image.getSubimage(PixelPosition.NinePlayers7Stack.getX(), PixelPosition.NinePlayers7Stack.getY(), PixelPosition.NinePlayers7Stack.getWidth(), PixelPosition.NinePlayers7Stack.getHeight());
            this.players.get(6).setStack(readNumberThroughOCR(player7Stack));
            BufferedImage player8Stack = image.getSubimage(PixelPosition.NinePlayers8Stack.getX(), PixelPosition.NinePlayers8Stack.getY(), PixelPosition.NinePlayers8Stack.getWidth(), PixelPosition.NinePlayers8Stack.getHeight());
            this.players.get(7).setStack(readNumberThroughOCR(player8Stack));
        }
    }

    public int readNumberThroughOCR(BufferedImage i) {
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(i, i);
        String s;
        try {
            s = tesseract.doOCR(i);
            if(s.equals("All-in")) return -2;
            List<String> allMatches = new ArrayList<>();
            Matcher m = Pattern.compile("\\d{2,}|\\d+\\.\\d{2,}")
                    .matcher(s);
            while (m.find()) {
                allMatches.add(m.group());
            }
            if(allMatches.size()>0){
                String text = allMatches.get(allMatches.size() - 1);
                String[] strings = text.split("\\.");
                return Integer.parseInt(Arrays.stream(strings).reduce("", (s1, s2) -> s1 + s2));
//                return Integer.parseInt(text);
            }
            else {
//                JFrame frame = new JFrame();
//                frame.getContentPane().setLayout(new FlowLayout());
//                frame.getContentPane().add(new JLabel(s));
//                frame.getContentPane().add(new JLabel(new ImageIcon(i)));
//                frame.pack();
//                frame.setVisible(true);
                return -1;
            }
        } catch (TesseractException e) {
            e.printStackTrace();
        }

//        JFrame frame = new JFrame();
//        frame.getContentPane().setLayout(new FlowLayout());
//        frame.getContentPane().add(new JLabel(s));
//        frame.getContentPane().add(new JLabel(new ImageIcon(i)));
//        frame.pack();
//        frame.setVisible(true);
        return -1;
    }

    public void acquirePot() {
        BufferedImage potImage = image.getSubimage(
                PixelPosition.Pot.getX(),
                PixelPosition.Pot.getY(),
                PixelPosition.Pot.getWidth(),
                PixelPosition.Pot.getHeight()
        );
        this.pot = readNumberThroughOCR(potImage);
    }

    public void acquireInfos(boolean cardsOnLeft, int numberOfTotalPlayers) {
        acquirePlayers(numberOfTotalPlayers);
        acquireHand(cardsOnLeft);
        acquireBoard();
        acquirePot();
        acquireBlinds();
    }

    private void acquireBlinds() {
        BufferedImage blindsImage = image.getSubimage(
                PixelPosition.Blinds.getX(),
                PixelPosition.Blinds.getY(),
                PixelPosition.Blinds.getWidth(),
                PixelPosition.Blinds.getHeight()
        );
        ColorConvertOp op = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        op.filter(blindsImage, blindsImage);
        try {
            String s = tesseract.doOCR(blindsImage);
            //            System.out.println(s);
            List<String> allMatches = new ArrayList<>();
            Matcher m = Pattern.compile("[\\d.]{2,}")
                    .matcher(s);
            while (m.find()) {
                allMatches.add(m.group());
            }
            //            System.out.println(allMatches);
            if(allMatches.size() == 0) this.bigBlind = 0;
            else {
                String blindsText = allMatches.get(allMatches.size() - 1);
                String[] strings = blindsText.split("\\.");
                this.bigBlind = Integer.parseInt(Arrays.stream(strings).reduce("", (s1, s2) -> s1 + s2));
            }
        } catch (TesseractException e) {
            e.printStackTrace();
        }
    }

    public void acquireBoard() {

        BufferedImage boardImage = image.getSubimage(
                PixelPosition.Board.getX(),
                PixelPosition.Board.getY(),
                PixelPosition.Board.getWidth(),
                PixelPosition.Board.getHeight()
        );

        BufferedImage firstCardOnBoardImage = boardImage.getSubimage(
                PixelPosition.BoardFirstCard.getX(),
                PixelPosition.BoardFirstCard.getY(),
                PixelPosition.BoardFirstCard.getWidth(),
                PixelPosition.BoardFirstCard.getHeight());
        BufferedImage firstCardOnBoardRankImage = firstCardOnBoardImage.getSubimage(
                PixelPosition.Rank.getX(),
                PixelPosition.Rank.getY(),
                PixelPosition.Rank.getWidth(),
                PixelPosition.Rank.getHeight());
        BufferedImage firstCardOnBoardSuitImage = firstCardOnBoardImage.getSubimage(
                PixelPosition.Suit.getX(),
                PixelPosition.Suit.getY(),
                PixelPosition.Suit.getWidth(),
                PixelPosition.Suit.getHeight());

        BufferedImage secondCardOnBoardImage = boardImage.getSubimage(
                PixelPosition.BoardSecondCard.getX(),
                PixelPosition.BoardSecondCard.getY(),
                PixelPosition.BoardSecondCard.getWidth(),
                PixelPosition.BoardSecondCard.getHeight());
        BufferedImage secondCardOnBoardRankImage = secondCardOnBoardImage.getSubimage(
                PixelPosition.Rank.getX(),
                PixelPosition.Rank.getY(),
                PixelPosition.Rank.getWidth(),
                PixelPosition.Rank.getHeight());
        BufferedImage secondCardOnBoardSuitImage = secondCardOnBoardImage.getSubimage(
                PixelPosition.Suit.getX(),
                PixelPosition.Suit.getY(),
                PixelPosition.Suit.getWidth(),
                PixelPosition.Suit.getHeight());

        BufferedImage thirdCardOnBoardImage = boardImage.getSubimage(
                PixelPosition.BoardThirdCard.getX(),
                PixelPosition.BoardThirdCard.getY(),
                PixelPosition.BoardThirdCard.getWidth(),
                PixelPosition.BoardThirdCard.getHeight());
        BufferedImage thirdCardOnBoardRankImage = thirdCardOnBoardImage.getSubimage(
                PixelPosition.Rank.getX(),
                PixelPosition.Rank.getY(),
                PixelPosition.Rank.getWidth(),
                PixelPosition.Rank.getHeight());
        BufferedImage thirdCardOnBoardSuitImage = thirdCardOnBoardImage.getSubimage(
                PixelPosition.Suit.getX(),
                PixelPosition.Suit.getY(),
                PixelPosition.Suit.getWidth(),
                PixelPosition.Suit.getHeight());

        BufferedImage fourthCardOnBoardImage = boardImage.getSubimage(
                PixelPosition.BoardFourthCard.getX(),
                PixelPosition.BoardFourthCard.getY(),
                PixelPosition.BoardFourthCard.getWidth(),
                PixelPosition.BoardFourthCard.getHeight());
        BufferedImage fourthCardOnBoardRankImage = fourthCardOnBoardImage.getSubimage(
                PixelPosition.Rank.getX(),
                PixelPosition.Rank.getY(),
                PixelPosition.Rank.getWidth(),
                PixelPosition.Rank.getHeight());
        BufferedImage fourthCardOnBoardSuitImage = fourthCardOnBoardImage.getSubimage(
                PixelPosition.Suit.getX(),
                PixelPosition.Suit.getY(),
                PixelPosition.Suit.getWidth(),
                PixelPosition.Suit.getHeight());

        BufferedImage fifthCardOnBoardImage = boardImage.getSubimage(
                PixelPosition.BoardFifthCard.getX(),
                PixelPosition.BoardFifthCard.getY(),
                PixelPosition.BoardFifthCard.getWidth(),
                PixelPosition.BoardFifthCard.getHeight());
        BufferedImage fifthCardOnBoardRankImage = fifthCardOnBoardImage.getSubimage(
                PixelPosition.Rank.getX(),
                PixelPosition.Rank.getY(),
                PixelPosition.Rank.getWidth(),
                PixelPosition.Rank.getHeight());
        BufferedImage fifthCardOnBoardSuitImage = fifthCardOnBoardImage.getSubimage(
                PixelPosition.Suit.getX(),
                PixelPosition.Suit.getY(),
                PixelPosition.Suit.getWidth(),
                PixelPosition.Suit.getHeight());
        this.board = new Board(
                new Card(firstCardOnBoardRankImage, firstCardOnBoardSuitImage),
                new Card(secondCardOnBoardRankImage, secondCardOnBoardSuitImage),
                new Card(thirdCardOnBoardRankImage, thirdCardOnBoardSuitImage),
                new Card(fourthCardOnBoardRankImage, fourthCardOnBoardSuitImage),
                new Card(fifthCardOnBoardRankImage, fifthCardOnBoardSuitImage)
        );
    }

    public void acquireHand(boolean cardsOnLeft) {
        if (cardsOnLeft) {
            firstCardRankImage = image.getSubimage(
                    PixelPosition.FirstCardRankLeft.getX(),
                    PixelPosition.FirstCardRankLeft.getY(),
                    PixelPosition.FirstCardRankLeft.getWidth(),
                    PixelPosition.FirstCardRankLeft.getHeight());
            firstCardSuitImage = image.getSubimage(
                    PixelPosition.FirstCardSuitLeft.getX(),
                    PixelPosition.FirstCardSuitLeft.getY(),
                    PixelPosition.FirstCardSuitLeft.getWidth(),
                    PixelPosition.FirstCardSuitLeft.getHeight());

            secondCardRankImage = image.getSubimage(
                    PixelPosition.SecondCardRankLeft.getX(),
                    PixelPosition.SecondCardRankLeft.getY(),
                    PixelPosition.SecondCardRankLeft.getWidth(),
                    PixelPosition.SecondCardRankLeft.getHeight());
            secondCardSuitImage = image.getSubimage(
                    PixelPosition.SecondCardSuitLeft.getX(),
                    PixelPosition.SecondCardSuitLeft.getY(),
                    PixelPosition.SecondCardSuitLeft.getWidth(),
                    PixelPosition.SecondCardSuitLeft.getHeight());
        } else {
            firstCardRankImage = image.getSubimage(
                    PixelPosition.FirstCardRank.getX(),
                    PixelPosition.FirstCardRank.getY(),
                    PixelPosition.FirstCardRank.getWidth(),
                    PixelPosition.FirstCardRank.getHeight());
            firstCardSuitImage = image.getSubimage(
                    PixelPosition.FirstCardSuit.getX(),
                    PixelPosition.FirstCardSuit.getY(),
                    PixelPosition.FirstCardSuit.getWidth(),
                    PixelPosition.FirstCardSuit.getHeight());

            secondCardRankImage = image.getSubimage(
                    PixelPosition.SecondCardRank.getX(),
                    PixelPosition.SecondCardRank.getY(),
                    PixelPosition.SecondCardRank.getWidth(),
                    PixelPosition.SecondCardRank.getHeight());
            secondCardSuitImage = image.getSubimage(
                    PixelPosition.SecondCardSuit.getX(),
                    PixelPosition.SecondCardSuit.getY(),
                    PixelPosition.SecondCardSuit.getWidth(),
                    PixelPosition.SecondCardSuit.getHeight());
        }
        this.hand = new Hand(new Card(this.getFirstCardRankImage(), this.getFirstCardSuitImage()),
                new Card(this.getSecondCardRankImage(), this.getSecondCardSuitImage()));
    }

    public BufferedImage getFirstCardRankImage() {
        return firstCardRankImage;
    }

    public BufferedImage getFirstCardSuitImage() {
        return firstCardSuitImage;
    }

    public static double getRGBDistance(BufferedImage imgA, BufferedImage imgB, int x, int y) {
        int rgbA = imgA.getRGB(x, y);
        int rgbB = imgB.getRGB(x, y);
        Color colorA = new Color(rgbA);
        Color colorB = new Color(rgbB);
        int r = colorA.getRed() - colorB.getRed();
        int rmean = (colorA.getRed() + colorB.getRed()) >> 1;
        int g = colorA.getGreen() - colorB.getGreen();
        int b = colorA.getBlue() - colorB.getBlue();
        return Math.sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
    }

    public static boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
        // The images must be the same size.
        double tol = 30;
        double distance;

        int width = imgA.getWidth();
        int height = imgA.getHeight();
        double totalTol = width * height * tol;
        double minimumTotalDistance = Double.MAX_VALUE;

        if (imgA.getWidth() == imgB.getWidth() && imgA.getHeight() == imgB.getHeight()) {
            double totalDistance = 0;
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    distance = getRGBDistance(imgA, imgB, x, y);
                    totalDistance += distance;
                }
            }
            if (totalDistance < minimumTotalDistance) minimumTotalDistance = totalDistance;
        } else {
            return false;
        }

        if (minimumTotalDistance < totalTol) {
//            if ( minimumTotalDistance > 0){
//                JFrame frame = new JFrame();
//                frame.getContentPane().setLayout(new FlowLayout());
//                frame.getContentPane().add(new JLabel(String.valueOf(minimumTotalDistance)));
//                frame.getContentPane().add(new JLabel(new ImageIcon(imgA)));
//                frame.getContentPane().add(new JLabel(new ImageIcon(imgB)));
//                frame.pack();
//                frame.setVisible(true);
//            }

            return true;
        }
        return false;
    }

    public BufferedImage getSecondCardRankImage() {
        return secondCardRankImage;
    }

    public BufferedImage getSecondCardSuitImage() {
        return secondCardSuitImage;
    }

    public void getScreenshotFromScreenshotNumber(int i) {
        try {
            File f = new File("screenshots/phoneScreenshot" + i + ".png");
            image = ImageIO.read(f);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int getPot() {
        return pot;
    }

    public Hand getHand() {
        return hand;
    }

    public Board getBoard() {
        return board;
    }

    public int getBigBlind() {
        return bigBlind;
    }

    public List<Player> getPlayers() {
        return players;
    }
}

