public enum Ranking {
    HIGH_CARD(1),
    PAIR(2),
    TWO_PAIR(3),
    THREE_OF_A_KIND(4),
    STRAIGHT(5),
    FLUSH(6),
    FULL_HOUSE(7),
    POKER(8),
    STRAIGHT_FLUSH(9);

    private Integer ranking;

    Ranking(Integer ranking){
        this.ranking = ranking;
    }

    public Integer getRanking() {
        return ranking;
    }

    public static Ranking getRankingFromInt(int i) {
        switch (i){
            case 1:
                return HIGH_CARD;
            case 2:
                return PAIR;
            case 3:
                return TWO_PAIR;
            case 4:
                return THREE_OF_A_KIND;
            case 5:
                return STRAIGHT;
            case 6:
                return FLUSH;
            case 7:
                return FULL_HOUSE;
            case 8:
                return POKER;
            case 9:
                return STRAIGHT_FLUSH;
            default:
                return null;
        }
    }
}
